About terminal:      

date     
listing files ls     
pwd     
~ represents home directory    
cd changing directories    
ls -la  (long format all files)    
creating files     
creating directories     
removing directories rmdir   
rm -i interactive    
copy cp     
viewing files cat     
less long text    
man ls help    

ls -la | less    
wildcards    
ls *.txt     
find    
find -name '*.txt' -print     

cd ~/Music find . -name *.mp3 -print > mymusic.txt    

grep    

find ~ -name '*' -print | grep readme    

wc   
wc sometext.txt    

changing permissions    

chmod u+w readme.txt     

shebang     
#!/Users/name/path/bin/ruby    
which    

environment    
env     


sudo command     
